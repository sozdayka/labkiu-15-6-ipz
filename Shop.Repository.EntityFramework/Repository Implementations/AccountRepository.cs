﻿using Shop.Model;
using Shop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Entity;

namespace Shop.Repository.EntityFramework
{
    public class AccountRepository : BasicRepository<Account>, IAccountRepository
    {
        public AccountRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.Accounts)
        {
        }
    }
}
