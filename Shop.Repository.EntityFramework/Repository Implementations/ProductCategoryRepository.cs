﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework
{
    public class ProductCategoryRepository : BasicRepository<ProductCategory>, IProductCategoryRepository
    {
        public ProductCategoryRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.ProductCategory)
        {
        }
    }
}