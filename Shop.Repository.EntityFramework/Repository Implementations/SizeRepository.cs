﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Shop.Repository.EntityFramework
{
    public class SizeRepository : BasicRepository<Size>, ISizeRepository
    {
        public SizeRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.Sizes)
        {
        }
    }
}
