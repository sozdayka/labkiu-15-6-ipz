﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework
{
    public class ColorRepository : BasicRepository<Color>, IColorRepository
    {
        public ColorRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.Colors)
        {
        }
    }
}
