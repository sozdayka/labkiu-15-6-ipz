﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework
{
    public class ProductRepository : BasicRepository<Product>, IProductRepository
    {
        public ProductRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.Products)
        {
        }
    }
}
