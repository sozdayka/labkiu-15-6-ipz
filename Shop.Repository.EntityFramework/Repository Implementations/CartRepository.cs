﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Shop.Repository.EntityFramework
{
    public class CartRepository : BasicRepository<Cart>, ICartRepository
    {
        public CartRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.Cart)
        {
        }
    }
}

