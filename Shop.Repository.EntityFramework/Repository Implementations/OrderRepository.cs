﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework
{
    public class OrderRepository : BasicRepository<Order>, IOrderRepository
    {
        public OrderRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.Orders)
        {
        }
    }
}
