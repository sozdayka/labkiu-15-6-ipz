﻿

using System.Data.Entity;
using System.Linq;
using Shop.Utils;

namespace Shop.Repository.EntityFramework
{
    public abstract class BasicRepository<T> where T : Entity
    {
        protected BasicRepository(ShopDbContext dbContext, DbSet<T> dbSet)
        {
            this.dbContext = dbContext;
            this.dbSet = dbSet;
        }

        protected ShopDbContext GetDBContext()
        {
            return this.dbContext;
        }

        protected DbSet GetDBSet()
        {
            return this.dbSet;
        }

        public void Add(T obj)
        {
            dbSet.Add(obj);
        }

        public void Delete(T obj)
        {
            dbSet.Remove(obj);
        }

        public void Commit()
        {
            dbContext.ChangeTracker.DetectChanges();
            dbContext.SaveChanges();
        }

        public IQueryable<T> LoadAll()
        {
            return dbSet;
        }

        public T Load(int id)
        {
            return dbSet.Find(id);
        }

        public int Count()
        {
            return dbSet.Count();
        }


        private ShopDbContext dbContext;
        private DbSet<T> dbSet;
    }
}
