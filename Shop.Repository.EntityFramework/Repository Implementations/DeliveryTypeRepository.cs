﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework
{
    public class DeliveryTypeRepository : BasicRepository<DeliveryType>, IDeliveryTypeRepository
    {
        public DeliveryTypeRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.DeliveryTypes)
        {
        }
    }
}
