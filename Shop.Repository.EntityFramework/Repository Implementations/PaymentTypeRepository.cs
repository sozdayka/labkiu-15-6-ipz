﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework
{
    public class PaymentTypeRepository : BasicRepository<PaymentType>, IPaymentTypeRepository
    {
        public PaymentTypeRepository(ShopDbContext dbContext)
            : base(dbContext, dbContext.PaymentTypes)
        {
        }
    }
}
