﻿using Shop.Model;

namespace Shop.Repository.EntityFramework.Configurations
{
    class CartConfiguration : BasicEntityConfiguration< Cart >
    {
        public CartConfiguration ()
        {
            HasMany< ProductItem >( c => c._items).WithRequired();
        }
    }
}
