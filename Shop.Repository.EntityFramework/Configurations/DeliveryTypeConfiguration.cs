﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework.Configurations
{
   
   class DeliveryTypeConfiguration : BasicEntityConfiguration<DeliveryType>
    {
        public DeliveryTypeConfiguration()
        {
            Property(i => i.DeliveryName).IsRequired();
            Property(i => i.DeliveryDescription).IsRequired();
        }
    }
}
