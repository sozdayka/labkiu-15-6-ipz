﻿using System.Data.Entity.ModelConfiguration;

namespace Shop.Repository.EntityFramework.Configurations
{
    abstract class BasicValueConfiguration< TValue > : ComplexTypeConfiguration< TValue > 
        where TValue : class
    {
        protected BasicValueConfiguration ()
        {
        }
    }
}
