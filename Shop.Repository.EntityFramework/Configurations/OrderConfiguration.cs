﻿using Shop.Model;

namespace Shop.Repository.EntityFramework.Configurations
{
    class OrderConfiguration : BasicEntityConfiguration< Order >
    {
        public OrderConfiguration ()
        {
            HasMany< ProductItem >( o => o.Items ).WithOptional();

            HasRequired(i => i.Delivery);
            HasRequired(i => i.Payment);
          
        }
    }
}
