﻿using Shop.Model;
using System.Data.Entity.ModelConfiguration;

namespace Shop.Repository.EntityFramework.Configurations
{
    class ProductPriceSizeConfiguration : EntityTypeConfiguration<ProductPriceSize>
    {
        public ProductPriceSizeConfiguration()
        {
            HasKey( c => c.Id);
            HasRequired( c => c._Product );
            HasRequired( c => c._Size );
        }
    }
}
