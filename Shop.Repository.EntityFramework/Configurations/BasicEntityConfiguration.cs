﻿using Shop.Utils;
using System.Data.Entity.ModelConfiguration;

namespace Shop.Repository.EntityFramework.Configurations
{
    abstract class BasicEntityConfiguration<T> : EntityTypeConfiguration<T>
        where T : Entity
    {
        protected BasicEntityConfiguration ()
        {
          // HasKey( e => e.DatabaseId);
           // Property( e => e.TableId).IsRequired();
        }
    }
}
