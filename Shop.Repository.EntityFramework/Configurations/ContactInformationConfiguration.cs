﻿using Shop.Model;

namespace Shop.Repository.EntityFramework.Configurations
{

    class ContactInformationConfiguration : BasicValueConfiguration< ContactInformation >
    {
        public ContactInformationConfiguration()
        {
            Property( c => c.Name).IsRequired();
            Property(c => c.Adress).IsRequired();
            Property( c => c.TelephoneNumber).IsRequired();
        }
    }
}
