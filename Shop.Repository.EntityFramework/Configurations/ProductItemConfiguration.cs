﻿using Shop.Model;

using System.Data.Entity.ModelConfiguration;

namespace Shop.Repository.EntityFramework.Configurations
{
    class ProductItemConfiguration : BasicEntityConfiguration<ProductItem>
    {
        public ProductItemConfiguration ()
        {
            HasRequired( i => i.SelectedProduct );
          
        }
    }
}
