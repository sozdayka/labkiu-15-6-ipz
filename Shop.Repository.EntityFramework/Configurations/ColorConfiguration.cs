﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework.Configurations
{ 
    class ColorConfiguration : BasicEntityConfiguration<Color>
    {
        public ColorConfiguration()
        {
            Property(i => i.ColorName).IsRequired();
        }
    }
}
