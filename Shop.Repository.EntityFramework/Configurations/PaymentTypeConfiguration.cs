﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework.Configurations
{
   
   class PaymentTypeConfiguration : BasicEntityConfiguration<PaymentType>
    {
        public PaymentTypeConfiguration()
        {
            Property(i => i.PaymentName).IsRequired();
            Property(i => i.PaymentDescription).IsRequired();
        }
    }
}
