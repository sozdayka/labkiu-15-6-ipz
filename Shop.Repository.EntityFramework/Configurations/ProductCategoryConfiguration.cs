﻿using Shop.Model;

namespace Shop.Repository.EntityFramework.Configurations
{
    class ProductCategoryConfiguration : BasicEntityConfiguration< ProductCategory >
    {
        public ProductCategoryConfiguration()
        {
            Property( s => s.CatagoryName).IsRequired();
        }
    }
}
