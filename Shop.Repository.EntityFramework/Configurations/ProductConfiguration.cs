﻿using Shop.Model;

namespace Shop.Repository.EntityFramework.Configurations
{
    class ProductConfiguration : BasicEntityConfiguration< Product >
    {
        public ProductConfiguration ()
        {
            Property( p => p.ProductName ).IsRequired();
            Property( p => p.ProductImageURL ).IsRequired();

            HasRequired(p => p.ProductCat);
           
            HasMany( p => p.ProductColors );

            HasMany(p => p.ProductPricesSize);
        }
    }
}
