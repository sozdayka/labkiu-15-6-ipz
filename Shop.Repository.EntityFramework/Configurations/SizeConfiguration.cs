﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework.Configurations
{ 
    class SizeConfiguration : BasicEntityConfiguration<Size>
    {
        public SizeConfiguration()
        {
            Property(i => i.SizeType).IsRequired();
        }
    }
}
