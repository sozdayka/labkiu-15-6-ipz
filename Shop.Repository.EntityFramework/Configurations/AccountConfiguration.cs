﻿/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

using Shop.Model;

namespace Shop.Repository.EntityFramework.Configurations
{
    class AccountConfiguration : BasicEntityConfiguration< Account >
    {
        public AccountConfiguration ()
        {
            Property(a => a.NickName).IsRequired();
            Property(a => a.Email).IsRequired();
            Property(a => a.Password).IsRequired();
        }
    }
}
