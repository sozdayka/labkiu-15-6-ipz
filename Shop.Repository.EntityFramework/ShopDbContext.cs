﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Data.Entity;
using Shop.Model;

namespace Shop.Repository.EntityFramework
{
    public class ShopDbContext : DbContext
    {
        public ShopDbContext():base("DbConfig")
        {
            /*Database.SetInitializer(
                new DatabaseInitializer()
            );*/
            Database.SetInitializer(
                new DropCreateDatabaseAlways<ShopDbContext>()
            );
        }

       /* public ShopDbContext()
        {
            Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));
        }*/

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategory { get; set; }
        public DbSet<DeliveryType> DeliveryTypes { get; set; }
        public DbSet<PaymentType> PaymentTypes { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Configurations.AccountConfiguration());
            modelBuilder.Configurations.Add(new Configurations.OperatorAccountConfiguration());
            modelBuilder.Configurations.Add(new Configurations.CartConfiguration());
            modelBuilder.Configurations.Add(new Configurations.OrderConfiguration());
            modelBuilder.Configurations.Add(new Configurations.ColorConfiguration());
            modelBuilder.Configurations.Add(new Configurations.ContactInformationConfiguration());

            modelBuilder.Configurations.Add(new Configurations.SizeConfiguration());
            modelBuilder.Configurations.Add(new Configurations.ProductConfiguration());
            modelBuilder.Configurations.Add(new Configurations.ProductItemConfiguration());
            modelBuilder.Configurations.Add(new Configurations.ProductCategoryConfiguration());
            modelBuilder.Configurations.Add(new Configurations.PaymentTypeConfiguration());
            modelBuilder.Configurations.Add(new Configurations.DeliveryTypeConfiguration());

            modelBuilder.Configurations.Add(new Configurations.ProductPriceSizeConfiguration());


        }
    }
}
