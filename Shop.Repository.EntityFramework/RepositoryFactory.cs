﻿using Shop.Repository.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository.EntityFramework
{
    public static class RepositoryFactory
    {
        public static IAccountRepository MakeAccountRepository(ShopDbContext dbContext)
        {
            return new AccountRepository(dbContext);
        }

        public static ICartRepository MakeCartRepository(ShopDbContext dbContext)
        {
            return new CartRepository(dbContext);
        }

        public static IColorRepository MakeColorRepository(ShopDbContext dbContext)
        {
            return new ColorRepository(dbContext);
        }

        public static IDeliveryTypeRepository MakeDeliveryTypeRepository(ShopDbContext dbContext)
        {
            return new DeliveryTypeRepository(dbContext);
        }

        public static IOrderRepository MakeOrderRepository(ShopDbContext dbContext)
        {
            return new OrderRepository(dbContext);
        }

        public static IPaymentTypeRepository MakePaymentTypeRepository(ShopDbContext dbContext)
        {
            return new PaymentTypeRepository(dbContext);
        }

        public static IProductCategoryRepository MakeProductCategoryRepository(ShopDbContext dbContext)
        {
            return new ProductCategoryRepository(dbContext);
        }

        public static IProductRepository MakeProductRepository(ShopDbContext dbContext)
        {
            return new ProductRepository(dbContext);
        }
        public static ISizeRepository MakeSizeRepository(ShopDbContext dbContext)
        {
            return new SizeRepository(dbContext);
        }

    }
}
