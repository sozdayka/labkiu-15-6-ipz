﻿using Shop.Repository;
using Shop.Repository.EntityFramework;
using System.Collections.Generic;

namespace Shop.TestApp
{
    class ModelLoader
    {
        public ModelLoader ( ShopDbContext dbContext )
        {
            this.dbContext = dbContext;
        }

        public ListModel Load ()
        {
            ListModel model = new ListModel();

            LoadCollection( RepositoryFactory.MakeSizeRepository( dbContext ),       model.Sizes );
            LoadCollection( RepositoryFactory.MakeColorRepository( dbContext ),        model.Colors );
            LoadCollection( RepositoryFactory.MakeProductCategoryRepository( dbContext ),           model.ProductCategories );
            LoadCollection( RepositoryFactory.MakeProductRepository( dbContext ),      model.Products );
            LoadCollection( RepositoryFactory.MakePaymentTypeRepository( dbContext ),             model.PaymentTypes );
            LoadCollection( RepositoryFactory.MakeDeliveryTypeRepository( dbContext ),           model.DeliveryTypes );
            LoadCollection( RepositoryFactory.MakeAccountRepository( dbContext ), model.Accounts );
            LoadCollection( RepositoryFactory.MakeCartRepository( dbContext ),          model.Carts );
            LoadCollection(RepositoryFactory.MakeOrderRepository(dbContext), model.Orders);
            return model;
        }

        private ShopDbContext dbContext;


        private void LoadCollection< TEntity >( IRepository< TEntity > repository, ICollection<TEntity> target)
            where TEntity : Utils.Entity
        {
            foreach ( TEntity obj in repository.LoadAll() )
                target.Add( obj );
        }
    }
}
