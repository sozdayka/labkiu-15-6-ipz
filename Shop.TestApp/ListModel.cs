﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.TestApp
{
    class ListModel
    {
        public ICollection<Account> Accounts { get; private set; }

        public ICollection<Size> Sizes { get; private set; }

        public ICollection<ProductCategory> ProductCategories { get; private set; }

        public ICollection<Product> Products { get; private set; }

        public ICollection<Color> Colors { get; private set; }

        public ICollection<Cart> Carts { get; private set; }

        public ICollection<Order> Orders { get; private set; }

        public ICollection<PaymentType> PaymentTypes { get; private set; }

        public ICollection<DeliveryType> DeliveryTypes { get; private set; }
        

        public ListModel()
        {
            this.Accounts = new List<Account>();
            this.Sizes = new List<Size>();
            this.Colors = new List<Color>();
            this.Products = new List<Product>();
            this.Carts = new List<Cart>();
            this.ProductCategories = new List<ProductCategory>();
            this.Orders = new List<Order>();
            this.PaymentTypes = new List<PaymentType>();
            this.DeliveryTypes = new List<DeliveryType>();
        }

    }
}
