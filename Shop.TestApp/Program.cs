﻿
using Shop.Repository.EntityFramework;
using Shop.TestApp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace Shop.TestApp
{


    class Program
    {


        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Directory.GetCurrentDirectory());

            try
            {
                ListModel model1 = DataTest.GenerateTestData();
                using (var dbContext = new ShopDbContext())
                {
                    ModelSaver saver = new ModelSaver(dbContext);
                    saver.Save(model1);
                }

                using (var dbContext = new ShopDbContext())
                {
                    ModelLoader restorer = new ModelLoader(dbContext);
                    ListModel model2 = restorer.Load();

                    CompareModels(model1, model2);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType().FullName);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }






            using (var ctx = new ShopDbContext())
            {
                using (var writer = new XmlTextWriter($@"{AppDomain.CurrentDomain.BaseDirectory}\ShopDBModel.edmx", Encoding.Default))
                {
                    EdmxWriter.WriteEdmx(ctx, writer);
                }
            }



        }

        private static void CompareModels(ListModel model1, ListModel model2)
        {
            StringWriter writer1 = new StringWriter(new StringBuilder());
            var reportGenerator1 = new ModelReporter(writer1);
            reportGenerator1.GenerateReport(model1);

            StringWriter writer2 = new StringWriter(new StringBuilder());
            var reportGenerator2 = new ModelReporter(writer2);
            reportGenerator2.GenerateReport(model2);

            String report1Content = writer1.ToString();
            String report2Content = writer2.ToString();

            Console.WriteLine(report1Content);

            Console.WriteLine("================================================================");
            Console.WriteLine();

            Console.WriteLine(report2Content);

            Console.WriteLine("================================================================");
            Console.WriteLine();

            Console.WriteLine(report1Content.Equals(report2Content) ? "PASSED: Models match" : "FAILED: Models mismatch");
        }
    }
}
