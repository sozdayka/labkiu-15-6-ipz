﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Shop.TestApp
{
    class ModelReporter
    {
        public ModelReporter ( TextWriter output )
        {
            this.output = output;
        }

        public void GenerateReport ( ListModel model )
        {
            ReportCollection( "Sizes", model.Sizes );
            ReportCollection( "Colors", model.Colors );
            ReportCollection("Categories", model.ProductCategories);
            ReportCollection( "Products", model.Products );
            ReportCollection( "Carts", model.Carts );
            ReportCollection( "Orders", model.Orders );
            ReportCollection( "Accounts", model.Accounts );
            ReportCollection("Deliveries", model.DeliveryTypes );
            ReportCollection( "Payments", model.PaymentTypes );
        }

        private void ReportCollection< T > ( string title, ICollection< T > items )
        {
            output.WriteLine("==== {0} ==== ", title );
            output.WriteLine();

            foreach ( var item in items )
            {
                output.WriteLine( item );
                output.WriteLine();
            }

            output.WriteLine();
        }

        private TextWriter output;
    }
}
