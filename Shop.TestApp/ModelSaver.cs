﻿using Shop.Model;
using Shop.Repository;
using Shop.Repository.EntityFramework;

using System.Collections.Generic;

namespace Shop.TestApp
{
    class ModelSaver
    {
        private ShopDbContext dbContext;
        public ModelSaver ( ShopDbContext dbContext )
        {
            this.dbContext = dbContext;
        }


        public void Save ( ListModel model )
        {
            
            SaveCollection(RepositoryFactory.MakeSizeRepository(dbContext), model.Sizes);
            SaveCollection(RepositoryFactory.MakeColorRepository(dbContext), model.Colors);
            SaveCollection(RepositoryFactory.MakeProductCategoryRepository(dbContext), model.ProductCategories);
            SaveCollection(RepositoryFactory.MakeProductRepository(dbContext), model.Products);
            SaveCollection(RepositoryFactory.MakePaymentTypeRepository(dbContext), model.PaymentTypes);
            SaveCollection(RepositoryFactory.MakeDeliveryTypeRepository(dbContext), model.DeliveryTypes);
            SaveCollection(RepositoryFactory.MakeAccountRepository(dbContext), model.Accounts);
            SaveCollection(RepositoryFactory.MakeCartRepository(dbContext), model.Carts);
            SaveCollection(RepositoryFactory.MakeOrderRepository(dbContext), model.Orders);
        }


        private void SaveCollection< TRepository, TEntity > ( TRepository repository, ICollection< TEntity > collection )
            where TRepository : IRepository< TEntity >
            where TEntity : Utils.Entity
        {
            foreach ( TEntity obj in collection )
                repository.Add( obj );

            repository.Commit();
        }

        
    }
}
