﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Shop.TestApp
{
    class Model
    {
        public Model(TextWriter output)
        {
            this.output = output;
        }

        public void GenerateReport(ListModel model)
        {
            ReportCollection("PaymentTypes", model.PaymentTypes);
            ReportCollection("DeliveryTypes", model.DeliveryTypes);
            ReportCollection("Accounts", model.Accounts);
            ReportCollection("Sizes", model.Sizes);
            ReportCollection("Categories", model.ProductCategories);
            ReportCollection("Products", model.Products);
            ReportCollection("Carts", model.Carts);
            ReportCollection("Orders", model.Orders);

            ReportToFile("PaymentTypes", model.PaymentTypes);
            ReportToFile("DeliveryTypes", model.DeliveryTypes);
            ReportToFile("Accounts", model.Accounts);
            ReportToFile("Sizes", model.Sizes);
            ReportToFile("Categories", model.ProductCategories);
            ReportToFile("Products", model.Products);
            ReportToFile("Carts", model.Carts);
            ReportToFile("Orders", model.Orders);

        }

        private void ReportCollection<T>(string title, ICollection<T> items)
        {
            output.WriteLine("==== {0} ==== ", title);
            output.WriteLine();

            foreach (var item in items)
            {
                output.WriteLine(item);
                output.WriteLine();
            }

            output.WriteLine();

      
        }
        private void ReportToFile<T>(string title, ICollection<T> items) {
            FileStream aFile = new FileStream(@"D:\OutTest.txt", FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(aFile);
            aFile.Seek(0, SeekOrigin.End);

            sw.WriteLine("==== {0} ==== ", title);
            sw.WriteLine();

            foreach (var item in items)
            {
                
                sw.WriteLine(item);
                sw.WriteLine();
            }

            sw.Close();
        }
        private TextWriter output;
    }
}
