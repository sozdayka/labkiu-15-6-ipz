﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Shop.Model;

namespace Shop.TestApp
{
    class DataTest
    {


        public static ListModel GenerateTestData()
        {
            ListModel m = new ListModel();

          
            GenerateSizes(m);
            GenerateColors(m);
            GeneratePaymentType(m);
            GenerateDeliveriesType(m);
            GenerateProductCategories(m);
            GenerateProducts(m);
            GenerateCarts(m);
            GenerateOrders(m);
            GenerateAccounts(m);

            return m;
        }


        private static void GenerateAccounts(ListModel m)
        {
            OperatorAccount Operator1 = new OperatorAccount(666, "razsas","vladfon@gmail.com","Vlad","Noname",99, "KGRAND CAYMAN ", "123qwerty");
            m.Accounts.Add(Operator1);

            OperatorAccount Operator2 = new OperatorAccount(666, "gosha", "thedoll@gosha.com", "Gosha", "Tralivov", 20, "Kiev,Barvinko 128 ", "gosha1SuperMen");
            m.Accounts.Add(Operator2);
           // Operator2.SetOrder(order);
        }

        private static void GenerateSizes(ListModel m)
        {
            SizeS = new Size(666, "S", true);
            SizeM = new Size(666, "M",true);
            SizeL = new Size(666, "L",true);
            SizeXXL = new Size(666, "XXL",true);


            m.Sizes.Add(SizeS);
            m.Sizes.Add(SizeM);
            m.Sizes.Add(SizeL);
            m.Sizes.Add(SizeXXL);
        }
        private static void GenerateColors(ListModel m)
        {
            Yellow = new Color(666, "Желтый", true);
            Blue = new Color(666, "Синий", true);
            Red = new Color(666, "Красный", true);
            White = new Color(666, "Белый", true);
            Pink = new Color(666, "Розовый", true);
            Black = new Color(666, "Черный", true);
            Green = new Color(666, "Зеленый", true);
            Purple = new Color(666, "Фиолетовый", true);

            m.Colors.Add(Yellow);
            m.Colors.Add(Blue);
            m.Colors.Add(Red);
            m.Colors.Add(White);
            m.Colors.Add(Pink);
            m.Colors.Add(Green);
            m.Colors.Add(Black);
            m.Colors.Add(Purple);
        }
        private static void GenerateProductCategories(ListModel m)
        {
            categoryJacket = new ProductCategory(666, "Верхняя одежда");
            categoryPants = new ProductCategory(666, "Штаны");
            categoryDress = new ProductCategory(666, "Платья");
           


            m.ProductCategories.Add(categoryJacket);
            m.ProductCategories.Add(categoryPants);
            m.ProductCategories.Add(categoryDress);
            
        }

        private static void GenerateDeliveriesType(ListModel m)
        {
            naSend = new DeliveryType(666, "Самовывоз","Самовывоз из нашего магазина №1");
            UkrPoshta = new DeliveryType(666, "UkrPoshta","Сервис отправки №1");

            m.DeliveryTypes.Add(naSend);
            m.DeliveryTypes.Add(UkrPoshta);
        }

        private static void GeneratePaymentType(ListModel m)
        {
            naPay = new PaymentType(666, "При получение","Отлата при получение товара");
            waitPay = new PaymentType(666, "Наложный платеж","Оплата на карту ПБ");

            m.PaymentTypes.Add(naPay);
            m.PaymentTypes.Add(waitPay);
        }
        


        private static void GenerateProducts(ListModel m)
        {
            // ----

            ShortDress = new Product(666, "Платье короткое");
            ShortDress.ProductImageURL = "http:";

            ShortDress.ProductCat = categoryDress;

           
            ShortDress.SetColor(Red);

            ShortDress.SetPrice(ShortDress,SizeS, 105.00M);
            ShortDress.SetPrice(ShortDress,SizeM, 107.00M);
            ShortDress.SetPrice(ShortDress,SizeL, 109.00M);

           

            Pants = new Product(666, "Штаны Офисные");
            Pants.ProductImageURL = "https:";

            Pants.ProductCat = categoryPants;

            Pants.SetColor(Blue);
       


            Pants.SetPrice(Pants,SizeS, 500.00M);
            Pants.SetPrice(Pants,SizeM, 500.00M);
            




            Jacket = new Product(666, "Куртка Красивая");
            Jacket.ProductImageURL = "http:";

            Jacket.ProductCat = categoryJacket;

            Jacket.SetColor(Black);
           

            Jacket.SetPrice(Jacket, SizeS, 300.00M);
            Jacket.SetPrice(Jacket, SizeM, 320.00M);
            Jacket.SetPrice(Jacket, SizeL, 350.00M);



            m.Products.Add(ShortDress);
            m.Products.Add(Pants);
            m.Products.Add(Jacket);
        }


        private static void GenerateCarts(ListModel m)
        {
            cart = new Cart(1);
            cart.AddItem(new ProductItem(666, ShortDress, ShortDress.GetPriceSize(SizeL), 1));
            cart.AddItem(new ProductItem(666, Jacket, Jacket.GetPriceSize(SizeM), 2));
            cart.AddItem(new ProductItem(666, Pants, Pants.GetPriceSize(SizeM), 1));

            cart.Checkout();

            m.Carts.Add(cart);
        }


        private static void GenerateOrders(ListModel m)
        {
            order = new Order(
                666,
                cart,
                new ContactInformation(666,"Вадим", "Харьков, Баварская 15", "+38(0123)1234567")
            );
            order.Payment = naPay;
            order.Delivery = naSend;

           
            order.Confirm();

            m.Orders.Add(order);
            
        }

        


        private static Size SizeS, SizeM, SizeL, SizeXXL;
        private static Color Yellow, Blue, Red, White, Black, Green, Purple, Pink;
        private static ProductCategory categoryJacket, categoryDress, categoryPants;
        private static Product ShortDress, Pants, Jacket;
        private static Cart cart;
        private static DeliveryType naSend,UkrPoshta;
        private static PaymentType naPay, waitPay;
        private static Order order;


    }
}
