﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Shop.Utils;

namespace Shop.Repository
{
    public interface IRepository<T> where T : Entity
    {
        int Count();

        T Load(int id);

        IQueryable<T> LoadAll();

        void Add(T t);

        void Delete(T t);

        void Commit();
    }
}
