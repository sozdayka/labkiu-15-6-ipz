﻿using Shop.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Repository
{

    public interface IProductCategoryRepository : IRepository<ProductCategory>
    {
    }
}
