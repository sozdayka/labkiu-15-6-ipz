﻿using System;
using Shop.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class Product : Entity
    {
        public int Id { get; set; }
        public string ProductName
        {
            get { return _ProductName.Value;}
            set { _ProductName.Value = value;}
        }
        public string ProductImageURL
        {
            get;set;
        }
        public ProductCategory ProductCat
        {
            get;
            set;
        }
        //public IDictionary<Size, decimal> ProductPrices
        //{
        //    get { return _ProductPrices; }
        //}

        public virtual ICollection<ProductPriceSize> _ProductPricesSize { get; private set; }

        //public ICollection<Size> ProductSizes
        //{
        //    get { return _ProductSizes; }
        //    set
        //    {
        //        this._ProductSizes = value.ToList();
        //    }
        //}

        public virtual ICollection<Color> ProductColors
        {
            get;set;
        }
        public ProductPriceSize GetPriceSize(Size T)
        {
           return ProductPricesSize.Where(p => p._Size.SizeType.Equals(T.SizeType)).First();
        }
        public void SetColor(Color Col)
        {

            ProductColors.Add(Col);
        }
        public Color GetColor(Color Col)
        {
            return Col;
        }
        protected Product() {  }
        public Product(int tableId, string _ProductName)
            
        {
            this.ProductName = _ProductName;
            this.ProductImageURL = "";
            this.ProductColors = new List<Color>();
           // this._ProductSizes = new List<Size>();
            this.ProductPricesSize = new List<ProductPriceSize>();
            this._ProductCat = ProductCat;
        }
        public override string ToString()
        {
            StringBuilder pricesAsString = new StringBuilder();
           
            foreach (var item in ProductPricesSize)
                pricesAsString.AppendFormat("{0} = {1}  ", item._Size.SizeType, item.Price);

            return string.Format(
                       "ID = {0}\nName = {1}\nImageUrl = {2}\nProductColors: {3}\nProductPrices: {4}\nProductCategory = {5}",
                       Id,
                       ProductName,
                       ProductImageURL,
                       String.Join(", ", ProductColors),
                       pricesAsString.ToString(),
                       ProductCat.CatagoryName
                   );
        }


        public decimal GetPrice ( Size size )
        {
        
            var res = ProductPricesSize.Where(p => p._Size.SizeType.Equals(size.SizeType)).First();
            if (res != null)
                return res.Price;

            throw new InvalidOperationException( "Price for size " + size.SizeType + " was not previously defined" );
        }

        public void SetPrice (Product product, Size size, decimal price )
        {
           

            ProductPricesSize.Add( new ProductPriceSize(product, size, price ));
        }

        public void RemovePrice ( Size size )
        {
            var priceAssignment = _ProductPricesSize.Where( a => a._Size == size ).FirstOrDefault();
            if ( priceAssignment != null )
                _ProductPricesSize.Remove( priceAssignment );

            else
                throw new InvalidOperationException( "No price defined for size " + size.SizeType );
        }



        private Utils.NonEmptyString _ProductName = new Utils.NonEmptyString("ProductName");
       
      //  private List<Size> _ProductSizes;
        //private List<Color> _ProductColors;
        private ProductCategory _ProductCat;
    }
}
