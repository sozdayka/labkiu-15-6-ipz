﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    
    public enum OrderStatus
    {
        OrderAdd,
        OrderCancel,
        OrderConfirm,
        OrderDelivering,
        OrderDelivered
    }
}
