﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class OperatorAccount : Account
        
    {
        public virtual ICollection<Order> Orders
        {
            get; set;
        }

        public void SetOrder(Order Order)
        {
            Orders.Add(Order);
        }
  
        protected OperatorAccount()
        {

        }
        public OperatorAccount(int tableId, string _NickName, string _Email, string _FirstName, string _SecondName, int _Age, string _Adress, string _Password)
           : base(tableId,  _NickName,  _Email,  _FirstName,  _SecondName,  _Age,  _Adress,  _Password)
        {
            this.Orders = new List<Order>();

        }
        

        public void CanselOrder(int TableId)
        {
            if (Orders == null)
                throw new ArgumentNullException("order");

            foreach (Order item in Orders)
            {
                if (item.Id == TableId)
                {
                    item.Status = OrderStatus.OrderCancel;
                }
            }
        }

        public void ConfirmOrder(int TableId)
        {
            if (Orders == null)
                throw new ArgumentNullException("order");

            foreach (Order item in Orders)
            {
                if (item.Id == TableId)
                {
                    item.Status = OrderStatus.OrderConfirm;
                }
            }
        }
        public Order ViewOrder(int TableId)
        {
            Order res = null;
            if (Orders == null)
                throw new ArgumentNullException("order");

            foreach (Order item in Orders)
            {
                if (item.Id == TableId)
                {
                    res = item;
                }
            }
            if (res == null)
                throw new InvalidOperationException("Order not found!");

            return res;
        }

        public List<Order> SearchOrders(OrderStatus status)
        {
            List<Order> res = null;
            if (Orders == null)
                throw new ArgumentNullException("order null");

            foreach (Order item in Orders)
            {
                if (item.Status == status)
                {
                    res.Add(item);
                }
            }
            if (res == null)
                throw new InvalidOperationException("Order not found!");

            return res;
        }

       

    }
}
