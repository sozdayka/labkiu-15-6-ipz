﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class ProductCategory : Entity
    {
        public int Id { get; set; }
        protected ProductCategory()
        {

        }
        public ProductCategory(int tableId, string _CatagoryName)
            
        {
            this.CatagoryName = _CatagoryName;
        }
        public string CatagoryName
        {
                get { return _CatagoryName.Value; }
                set { _CatagoryName.Value = value; }
        }
        private Utils.NonEmptyString _CatagoryName = new Utils.NonEmptyString("CatagoryName");
        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nCategoryName = {1}",
                       Id,
                       CatagoryName
                   );
        }
    }
   
}
