﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class ProductItem : Entity
    {
        public int Id { get; set; }
        protected ProductItem(){}
        public Product SelectedProduct
        {
            get { return _product.Value; }
            private set { _product.Value = value; }
        }

        public ProductPriceSize ProductPS
        {
            get { return _sizePrice.Value; }
            private set { _sizePrice.Value = value; }
        }

        public decimal FixedPrice
        {
            get { return _fixedPrice.Value; }
            private set { _fixedPrice.Value = value; }
        }

        public int Quantity
        {
            get { return _quantity.Value; }
            private set { _quantity.Value = value; }
        }

        public decimal Cost
        {
            get
            {
                return FixedPrice * Quantity;
            }
        }

        public ProductItem(int tableId, Product product, ProductPriceSize sizeItem, int quantity) 
            
        {
            if (quantity <= 0)
                throw new Exception("ProductItem: non-positive quantity");

            this._product.Value = product;
            this._sizePrice.Value = sizeItem;

            this._fixedPrice.Value = product.GetPriceSize(_sizePrice.Value._Size).Price;
            this._quantity.Value = quantity;
        }

        public override string ToString()
        {
            return string.Format(
                       "Product = {0}, Size = {1}, Price = {2}, Quantity = {3}",
                       SelectedProduct.ProductName,
                       ProductPS._Size.SizeType,
                       ProductPS.Price,
                       Quantity
                   );
        }


        private readonly Utils.RequiredProperty<Product> _product =
            new Utils.RequiredProperty<Product>("product");

        private readonly Utils.RequiredProperty<ProductPriceSize> _sizePrice =
            new Utils.RequiredProperty<ProductPriceSize>("sizePrice");

        private readonly Utils.RangeProperty<int> _quantity =
            new Utils.RangeProperty<int>("quantity", 0, false, int.MaxValue, true);

        private readonly Utils.RangeProperty<decimal> _fixedPrice =
            new Utils.RangeProperty<decimal>("fixedPrice", 0, true, decimal.MaxValue, true);
    }
}
