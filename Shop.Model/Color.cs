﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
   public class Color:Entity
    {
        public int Id { get; set; }
        public string ColorName
        {
            get { return _ColorName.Value; }
            set { _ColorName.Value = value; }
        }
        public bool Visible
        {
            get;set;
        }
        protected Color()
        {

        }
        public Color(int tableId, string _ColorName, bool _Visible=true)
            
        {
            this.ColorName = _ColorName;
            this.Visible = _Visible;
        }
        private Utils.NonEmptyString _ColorName = new Utils.NonEmptyString("ColorName");

        public override string ToString()
        {
            return string.Format(
                       "{0}, {1}",
                      Id,
                       ColorName
                      
                   );
        }
    }


}
