﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class PaymentType : Entity
    {
        public int Id { get; set; }
        public string PaymentName
        {
            get { return _PaymentName.Value; }
            set { _PaymentName.Value = value; }
        }
        public string PaymentDescription
        {
            get { return _PaymentDescription.Value; }
            set { _PaymentDescription.Value = value; }
        }
        protected PaymentType()
        {

        }
        public PaymentType(int tableId, string _PaymentName, string _PaymentDescription)
            
        {
            this.PaymentName = _PaymentName;
            this.PaymentDescription = _PaymentDescription;
        }


        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nPaymentName = {1}\nPaymentDescription = {2}",
                       Id,
                       PaymentName,
                       PaymentDescription
                   );
        }
        private Utils.NonEmptyString _PaymentName = new Utils.NonEmptyString("PaymentName");
        private Utils.NonEmptyString _PaymentDescription = new Utils.NonEmptyString("PaymentDescription");
    }
}