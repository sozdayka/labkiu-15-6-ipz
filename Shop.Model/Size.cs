﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class Size : Entity
    {
        public int Id { get; set; }
        public string SizeType
        {
            get { return _SizeType.Value; }
            set { _SizeType.Value = value; }
        }
        public bool Visible
        {
            get; set;
        }
        public Size()
        {

        }
        public Size(int tableId, string _SizeType, bool _Visible)
            
        {
            this.SizeType = _SizeType;
            this.Visible = _Visible;
        }
        private Utils.NonEmptyString _SizeType = new Utils.NonEmptyString("SizeType");

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nSizeType = {1}\nVisible = {2}",
                       Id,
                       SizeType,
                       Visible
                   );
        }
    }
}