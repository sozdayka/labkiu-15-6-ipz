﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class Order : Entity
    {
        public int Id { get; set; }
        public PaymentType Payment
        {
            get; set;

        }
        public DeliveryType Delivery
        {
            get;set;
        }
        public virtual ICollection<ProductItem> Items { get; private set; }


        public decimal TotalCost {get; private set;}

        public OrderStatus Status { get; set; }


        public ContactInformation customerContact
        {
            get { return _ContactInformation.Value; }
            private set { _ContactInformation.Value = value; }
        }

      
        protected Order()
        {

        }
        public Order(int tableId, Cart cart, ContactInformation contact)
            
        {
            if (cart.Modifiable)
                throw new InvalidOperationException("Order: initializing with a modifiable cart");

            this._ContactInformation.Value = contact;
            this.Items = new List<ProductItem>(cart._items);

            this._deliveryType = Delivery;
            this._paymentType =  Payment;


            this.Status = OrderStatus.OrderAdd;


            this.TotalCost = 0;
            foreach (ProductItem item in Items)
                this.TotalCost += item.Cost;
        }
      

        public void Confirm()
        {
            if (Status != OrderStatus.OrderAdd)
                throw new InvalidOperationException("Order.Confirm - can only run in Placed state");

            Status = OrderStatus.OrderConfirm;
        }

        public void Cancel()
        {
            if (Status != OrderStatus.OrderAdd)
                throw new InvalidOperationException("Order.Cancel - can only happen in Cancel state");

            Status = OrderStatus.OrderCancel;
        }
        public void Delivering()
        {
            if (Status != OrderStatus.OrderConfirm)
                throw new InvalidOperationException("Order.OrderDelivering - can only happen in Delivering state");

            Status = OrderStatus.OrderDelivering;
        }
        public void Delivered()
        {
            if (Status != OrderStatus.OrderDelivering)
                throw new InvalidOperationException("Order.OrderDelivered - can only happen in Delivered state");

            Status = OrderStatus.OrderDelivered;
        }

        public override string ToString()
        {
            StringBuilder ItemsAsString = new StringBuilder();
            foreach (var entry in Items)
                ItemsAsString.AppendFormat("\n{0}, Size - {1}, Cost - {2}, Amount - {3}\n", entry.SelectedProduct.ProductName, entry.ProductPS._Size.SizeType, entry.ProductPS.Price, entry.Quantity);
            return string.Format(
                       "ID = {0}\nContactInformation:\n{1}\nProducts = {2}\nTotalCost= {3}\nOrderStatus= {4}",
                       Id,
                       customerContact,
                       ItemsAsString.ToString(),
                       TotalCost,
                       Status
                   );
        }

        private readonly List<ProductItem> _items;
        private  DeliveryType _deliveryType;
        private  PaymentType _paymentType;
      
        private Utils.RequiredProperty<ContactInformation> _contact = new Utils.RequiredProperty<ContactInformation>("ContactInformation");

        private Utils.RequiredProperty<ContactInformation> _ContactInformation = new Utils.RequiredProperty<ContactInformation>("contact");
    }
}
