﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class Account: Entity
    {
       public int Id { get; set; }
        public string NickName
        {
            get { return _nickName.Value; }
            set { _nickName.Value = value; }
        }

        public string Email
        {
            get { return _email.Value; }
            set { _email.Value = value; }
        }
        public string FirstName
        {
            get { return _firstName.Value; }
            set { _firstName.Value = value; }
        }

        public string SecondName
        {
            get { return _secondName.Value; }
            set { _secondName.Value = value; }
        }

        public int Age { get; set; }

        public string Adress { get; set; }

        public string Password { get; set; }
        protected Account() { }

        public Account(int tableId, string _NickName, string _Email, string _FirstName, string _SecondName, int _Age, string _Adress, string _Password)
            
        {
            this.NickName = _NickName;
            this.Email = _Email;
            this.FirstName = _FirstName;
            this.SecondName = _SecondName;
            this.Age = _Age;
            this.Adress = _Adress;
            if (!CheckPassword(_Password))
            {
                this.Password = _Password;
            }
        }

    
        public bool CheckPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            return this.Password == password;
        }

        public void ChangeIfNotNull(string paramName, string paramNameToChange)
        {
            if (paramName != null)
                paramNameToChange = paramName;

        }

        public void ChangePassword(string password)
        {
            this.Password = password;
        }

    public void ChangeAccountInfo(string _NickName, string _Email, string _FirstName, string _SecondName, int _Age, string _Adress, string _Password)
            
        {
            ChangeIfNotNull(_NickName, this.NickName);
            ChangeIfNotNull(_FirstName, this.FirstName);
            ChangeIfNotNull(_SecondName, this.SecondName);
            ChangeIfNotNull(_Adress, this.Adress);
            ChangeIfNotNull(_NickName, this.NickName);

            this.Email = _Email;
            this.FirstName = _FirstName;
            this.SecondName = _SecondName;
            this.Age = _Age;
            this.Adress = _Adress;
            CheckPassword(_Password);
    }
        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nNickName = {1}\nEmail = {2}\nFirstName = {3}\nSecondName = {4}\nAge = {5}\nAdress = {6}\nPassword = {7}",
                       Id,
                       NickName,
                       Email,
                       FirstName,
                       SecondName,
                       Age,
                       Adress,
                       Password
                   );
        }

        private Utils.NonEmptyString _nickName = new Utils.NonEmptyString("NickName");
        private Utils.NonEmptyString _firstName = new Utils.NonEmptyString("FirstName");
        private Utils.NonEmptyString _secondName = new Utils.NonEmptyString("SecondName");
        private Utils.RegexString _email = new Utils.RegexString("email", "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
        private Utils.NonEmptyString _password = new Utils.NonEmptyString("password");

    }
}
