﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class Cart : Utils.Entity
    {
        public int Id { get; set; }
        public virtual IList<ProductItem> _items { get; private set; }

        protected Cart() { }
        public bool Modifiable { get; private set; }

        public decimal Cost
        {
            get
            {
                decimal totalCost = 0;
                for (int i = 0; i < _items.Count; i++)
                    totalCost += _items[i].ProductPS.Price;
                return totalCost;
            }
        }


        public Cart(int tableId)
            
        {
            this._items = new List<ProductItem>();
            this.Modifiable = true;
        }


        public override string ToString()
        {
            StringBuilder b = new StringBuilder();

            b.AppendFormat("tableId = {0}\n", Id);

            b.AppendLine("Content:");
            foreach (var item in _items)
            {
                b.Append("\t");
                b.Append(item);
                b.AppendLine();
            }

            b.AppendFormat("Modifiable = {0}\n", Modifiable);
            b.AppendFormat("TotalCost = {0}\n", Cost);
            return b.ToString();
        }


        public int FindItemIndex(Product product, ProductPriceSize ProductSize)
        {
            for (int i = 0; i < _items.Count; i++)
            {
                ProductItem item = _items[i];
                if (item.SelectedProduct == product && item.ProductPS == ProductSize)
                    return i;
            }

            return -1;
        }


        public void AddItem(ProductItem item)
        {
            if (!Modifiable)
                throw new InvalidOperationException("ShoppingCart.AddItem: unmodifiable cart");

            int existingItemIndex = FindItemIndex(item.SelectedProduct, item.ProductPS);
            if (existingItemIndex != -1)
                throw new InvalidOperationException("ShoppingCart.AddItem: duplicate product-size pair added");

            _items.Add(item);
        }


        public void UpdateItem(int index, ProductItem item)
        {
            if (!Modifiable)
                throw new InvalidOperationException("ShoppingCart.UpdateItem: unmodifiable cart");

            _items[index] = item;
        }


        public void DropItem(int index)
        {
            if (!Modifiable)
                throw new InvalidOperationException("ShoppingCart.DropItem: unmodifiable cart");

            _items.RemoveAt(index);
        }


        public void ClearItems()
        {
            if (!Modifiable)
                throw new InvalidOperationException("ShoppingCart.ClearItems: unmodifiable cart");

            _items.Clear();
        }


        public void Checkout()
        {
            if (_items.Count == 0)
                throw new InvalidOperationException("ShoppingCart.Lock: locking empty cart");

            Modifiable = false;
        }


       
    }
}

