﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class ProductPriceSize 
    {
        public int Id { get; set; }
        public long PriceSizeId { get; set; }

        public virtual Product _Product { get; private set; }

        public virtual Size _Size { get; private set; }

        public decimal Price
        {
            get { return _price.Value; }
            set { _price.Value = value; }
        }

        protected ProductPriceSize() { }

        public ProductPriceSize(Product product, Size size, decimal price)
        {
            
            this._Product = product;
            this._Size = size;
            this._price.Value = price;
        }

        public override string ToString()
        {
            return string.Format(
                       "Prize= {0} Size = {1}",
                  
                      
                       Price,
                       _Size.SizeType

                   );
        }
        private Utils.RangeProperty<decimal> _price =
            new Utils.RangeProperty<decimal>("price", 0.00M, true, decimal.MaxValue, false);
    }
}
