﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class DeliveryType:Entity
    {
        public int Id { get; set; }
        public string DeliveryName
        {
            get { return _DeliveryName.Value; }
            set { _DeliveryName.Value = value; }
        }
        public string DeliveryDescription
        {
            get { return _DeliveryDescription.Value; }
            set { _DeliveryDescription.Value = value; }
        }
        protected DeliveryType()
        {

        }
        public DeliveryType(int tableId, string _DeliveryName,string _DeliveryDescription)
            
        {
            this.DeliveryName = _DeliveryName;
            this.DeliveryDescription = _DeliveryDescription;
        }

        public override string ToString()
        {
            return string.Format(
                       "ID = {0}\nDeliveryName = {1}\nDeliveryDescription = {2}",
                       Id,
                       DeliveryName,
                       DeliveryDescription
                   );
        }
        
        private Utils.NonEmptyString _DeliveryName = new Utils.NonEmptyString("DeliveryName");
        private Utils.NonEmptyString _DeliveryDescription = new Utils.NonEmptyString("DeliveryDescription");
    }
}
