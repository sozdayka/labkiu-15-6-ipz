﻿using Shop.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shop.Model
{
    public class ContactInformation :  Value<ContactInformation>
    {
        public int Id { get; set; }
        public string Name
        {
            get { return _Name.Value; }
            set { _Name.Value = value;}
        }

        public string Adress
        {
            get { return _Adress.Value; }
            set { _Adress.Value = value; }
        }

        public string TelephoneNumber
        {
            get { return _TelephoneNumber.Value; }
            set { _TelephoneNumber.Value = value; }
        }
        protected ContactInformation()
        {

        }
        public ContactInformation(int tableId, string _Name, string _Adress, string _TelephoneNumber)
    
           
        {
            this.Name = _Name;
            this.Adress = _Adress;
            this.TelephoneNumber = _TelephoneNumber;
        }

        public override string ToString()
        {
            return string.Format(
                       "Name = {0}\nTelephoneNumber = {1}\nAdress = {2}",
                       
                       Name,
                       TelephoneNumber,
                       Adress
                   );
        }
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            return new object[] { _Name, _Adress, _TelephoneNumber };
        }
        private Utils.NonEmptyString _Name = new Utils.NonEmptyString("Name");
        private Utils.NonEmptyString _Adress = new Utils.NonEmptyString("Adress");
        private Utils.RegexString _TelephoneNumber = new Utils.RegexString("TelephoneNumber", "^((8|\\+38)[\\- ]?)?(\\(?\\d{3,4}\\)?[\\- ]?)?[\\d\\- ]{5,10}$");
    }
}
